package com.app.dummyapiimplementation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.dummyapiimplementation.databinding.BookItemBinding
import com.app.dummyapiimplementation.mvvm.model.Book

class MainAdapter : RecyclerView.Adapter<MainViewHolder>() {

    var books = mutableListOf<Book>()
    private lateinit var binding: BookItemBinding
    fun setBookList(books: List<Book>) {
        this.books = books.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = BookItemBinding.inflate(inflater, parent, false)

        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val book = books[position]
        holder.binding.tvId.text = "ID : " + book.id
        holder.binding.tvBookName.text = "Book Name : " + book.bookname
        holder.binding.tvAuthor.text = "Author : " + book.author


    }

    override fun getItemCount(): Int {
        return books.size
    }
}

class MainViewHolder(val binding: BookItemBinding) : RecyclerView.ViewHolder(binding.root) {

}