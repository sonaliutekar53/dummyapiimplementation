package com.app.dummyapiimplementation.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}