package com.app.dummyapiimplementation.Repository

import com.app.dummyapiimplementation.network.ApiService

class MainRepository constructor(private val retrofitService: ApiService) {

    fun getAllBooks() = retrofitService.getBooks()
}