package com.app.dummyapiimplementation.network

import com.app.dummyapiimplementation.mvvm.model.Book
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {
    @GET("bookapi")
    fun getBooks(): Call<List<Book>>


    companion object {

        var retrofitService: ApiService? = null

        fun getInstance() : ApiService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                        .baseUrl("https://8de1151a-ebfa-4ecd-b332-26623f896353.mock.pstmn.io/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                retrofitService = retrofit.create(ApiService::class.java)
            }
            return retrofitService!!
        }
    }
}