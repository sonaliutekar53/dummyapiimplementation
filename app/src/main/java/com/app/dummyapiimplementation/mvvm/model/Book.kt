package com.app.dummyapiimplementation.mvvm.model

import com.google.gson.annotations.SerializedName

data class Book (
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("bookname")
    val bookname: String = "",
    @SerializedName("author")
    val author: String = ""
)