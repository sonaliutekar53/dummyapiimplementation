package com.app.dummyapiimplementation.mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.dummyapiimplementation.Repository.MainRepository
import com.app.dummyapiimplementation.mvvm.model.Book
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainViewModel constructor(private val repository: MainRepository)  : ViewModel() {

    val booksList = MutableLiveData<List<Book>>()
    val errorMessage = MutableLiveData<String>()

    fun getAllBooks() {

        val response = repository.getAllBooks()
        response.enqueue(object : Callback<List<Book>> {
            override fun onResponse(call: Call<List<Book>>, response: Response<List<Book>>) {
                booksList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<Book>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}