package com.app.dummyapiimplementation.mvvm.view

import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.dummyapiimplementation.R
import com.app.dummyapiimplementation.Repository.MainRepository
import com.app.dummyapiimplementation.adapter.MainAdapter
import com.app.dummyapiimplementation.databinding.ActivityMainBinding
import com.app.dummyapiimplementation.mvvm.viewmodel.MainViewModel
import com.app.dummyapiimplementation.mvvm.viewmodel.MyViewModelFactory
import com.app.dummyapiimplementation.network.ApiService

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    private val retrofitService = ApiService.getInstance()
    val adapter = MainAdapter()
    lateinit var progressBar: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel =
            ViewModelProvider(this, MyViewModelFactory(MainRepository(retrofitService))).get(
                MainViewModel::class.java
            )

        binding.recyclerview.adapter = adapter

        viewModel.booksList.observe(this, Observer {
            adapter.setBookList(it)
            if (progressBar.isShowing) {
                progressBar.dismiss()
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            if (progressBar.isShowing) {
                progressBar.dismiss()
            }
            Toast.makeText(this, "Please try again later", Toast.LENGTH_LONG).show()
        })


        if (isNetworkAvailable()) {
            progressBar = ProgressDialog(this)
            progressBar.setCancelable(true) //you can cancel it by pressing back button
            progressBar.setMessage("Please wait ...")
            progressBar.show()
            viewModel.getAllBooks()
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show()
        }
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}